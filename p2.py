#!/usr/bin/python
import ssl
import sys, httplib, getopt, os 
#from uuid import uuid4
#from datetime import datetime
from OpenSSL import crypto
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from hashlib import md5



'''
 convert your pkcs12 to pem for using with zk
 openssl pkcs12 -info -in mycert2.pfx.pem

'''


env_host=''
env_post=''
env_cacert=''

def env_prod():
    global env_host
    env_host='cis.porezna-uprava.hr'
    global env_post
    env_post='/FiskalizacijaService'
    global env_cacert
    env_cacert='certs/prodCAfile.pem'

def env_test():
    global env_host
    env_host='cistest.apis-it.hr'
    global env_post
    env_post='/FiskalizacijaServiceTest'
    global env_cacert
    env_cacert='certs/demoCAfile.pem'

def return_help():
    print '''
Usage -h =this screen
    -z/-zk oib;datumVrijeme;brRacuna;ozPoslovnogP;ozUredaja;ukupnoIznos;key_filename;key_password = zg generator
    -s/-sm xml_file = sending xml file to fina-fiskal
    '''

def read_arg(argv):
    try:
        opts, args = getopt.getopt(argv,"h:z:s:",["","zk=","send="])
    except getopt.GetoptError:
        return_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h',"--help"):
            return_help()
            sys.exit(0)
        elif opt in ("-z", "--zk"):
            if (len(arg.split(";")) == 8):
                zk=arg.split(";")
                print (zk_gen(zk[0],zk[1],zk[2],zk[3],zk[4],zk[5],zk[6],zk[7]))
            else:
                print "we need for zk 8param"
                return_help
                sys.exit(2)
            
        elif opt in ("-s","--sm"):
            send_soap(arg)
        else:
            #return_help()
            #sys.exit(2)
            send_soap(arg)


def send_soap(xml_file):
    if os.path.isfile(xml_file):
        xmlf = open(xml_file,"r")
        SoapMessage = xmlf.read()
    #    ssl.PROTOCOL_TLSv1_2
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ctx.options |= ssl.OP_NO_SSLv2
        ctx.options |= ssl.OP_NO_SSLv3
        ctx.options |= ssl.OP_NO_TLSv1
        ctx.verify_mode = ssl.CERT_REQUIRED
        ctx.load_default_certs()
#        ctx.load_verify_locations("certs/prodCAfile.pem")
        ctx.load_verify_locations(env_cacert)
        #cafile = "certs/prodCAfile.pem"
        cafile = env_cacert
        verify = cafile
        webservice = httplib.HTTPSConnection(env_host, 8449, timeout=10,context=ctx)
        webservice.putrequest("POST", env_post)
        webservice.putheader("Host", env_host)
        #webservice.putheader("User-Agent", "test-post")
        webservice.putheader("Content-type", "text/xml; charset=\"UTF-8\"")
        webservice.putheader("Content-length", "%d" % len(SoapMessage))
        webservice.putheader("SOAPAction", "\"\"")
        webservice.endheaders()
        webservice.send(SoapMessage)
        resp = webservice.getresponse()
        res = resp.read()
        print(res)
    else:
        print "File \"%s\" doesn't exists" % xml_file



def zk_gen(oib, datumVrijeme, brRacuna, ozPoslovnogP, ozUredaja, ukupnoIznos, key_filename, key_password):
    forsigning = oib + datumVrijeme + brRacuna + ozPoslovnogP + ozUredaja + ukupnoIznos
    #print forsigning
    key_pem = open(key_filename).read()
    if key_password:
        pkey = crypto.load_privatekey(crypto.FILETYPE_PEM, key_pem,key_password)
    else:
        pkey = crypto.load_privatekey(crypto.FILETYPE_PEM, key_pem)
    signature = crypto.sign(pkey, forsigning, 'sha1')
    signature = md5(signature).hexdigest()
    return signature



if __name__ == "__main__":
#-----
# here call for which env env_prod-produciont env_test-test 
    env_test()
#    env_prod()
#-----
    if (read_arg(sys.argv[1:])):
        c=''
    else:
        #return_help()
        if (len(sys.argv)==2):
            send_soap(sys.argv[1])
        sys.exit(2)

